	#ifndef _ANALYSEUR_HPP_
	#define _ANALYSEUR_HPP_

	#include <iostream>
	#include <fstream>
	#include <essentia/algorithmfactory.h>
	#include <essentia/pool.h>
	#include <essentia/utils/synth_utils.h>
	#include <unistd.h>
	#include <sys/stat.h>
	#include <dirent.h>

	using namespace std;
	using namespace essentia;
	using namespace standard;
	using namespace essentia::standard;
	using namespace essentia::scheduler;

	//!  Classe Analyseur. 
	/*!
	  Cette classe regroupe toutes les fonctions d'analyse musicale.
	*/
	struct Analyseur
	{
		//!  mono. 
		/*!
		  Contient les données de la musique.
		*/
		vector<Real> mono;

		//!  left. 
		/*!
		  Contient les données de la musique qui sont à gauche.
		*/
		vector<Real> left;

		//!  right. 
		/*!
		  Contient les données de la musique qui sont à droite.
		*/
		vector<Real> right;

	//!  Un constructeur. 
		/*!
		  On l'initialise.
		*/
	Analyseur(){
		essentia::init();
	}

	//!  Un destructeur. 
		/*!
		  On l'éteint.
		*/
	~Analyseur(){
		essentia::shutdown();
	}

	public:

		/////////// DECLARATION DES STRUCTURES DE DONNES ///////////

		//!  Structure MetaData. 
		/*!
		  Contient les méta données d'une musique, tel que le titre, l'artiste ou l'album.
		*/
		struct metaData
		{
		public:
			string title, artist, album, comment, genre, tracknumber, date;
			int duration, bitrate, sampleRate, channels;

			//!  Constructeur par défaut. 
			/*!
			  Voici le construteur par défaut.
			*/
			metaData(){}

			//!  Un autre contructeur. 
			/*!
			  Ce constructeur prend en paramètre toutes les méta données de la chanson.
			*/
			metaData(string _title, string _artist, string _album, string _comment, string _genre, string _tracknumber, string _date, int _duration, int _bitrate, int _sampleRate, int _channels):
				title(_title), artist(_artist), album(_album), comment(_comment), genre(_genre), tracknumber(_tracknumber), date(_date), duration(_duration), bitrate(_bitrate), sampleRate(_sampleRate), channels(_channels)
				{}

			//!  Fonction d'affichage. 
			/*!
			  Affiche toutes les méta données de la chanson.
			*/
			void affichage(){
				cout << "--------------------------  results --------------------------" << endl;

				cout << "titre : " << title << endl <<
				    "artiste : " << artist << endl <<
				    "album : " << album << endl <<
				    "comment : " << comment << endl <<
				    "genre : " << genre << endl <<
				    "tracknumber : " << tracknumber << endl <<
				    "date : " << date << endl <<
				    "duration : " << duration << endl <<
				    "bitrate : " << bitrate << endl <<
				    "sampleRate : " << sampleRate << endl <<
				    "channels : " << channels << endl;
			}
		};

		//!  Les données. 
		/*!
		  Correspond au méta données de la musique.
		*/
		metaData datas;

		//!  Structure des fréquences potentielles. 
		/*!
		  Correspond aux fréquences qui apparaissent dans la musique,
		  on conmpte leur nombre d'occurences et si elles atteignent un seuil elles sont enregistrées.
		*/
		struct FrequencePotentielle{
		private:
			//!  frequence. 
			/*!
			  La valeur de la fréquence.
			*/
			Real frequence;

			//!  nbOccurences. 
			/*!
			  Son nombre d'occurence.
			*/
			int nbOccurences;

			//!  numFrameDepart. 
			/*!
			  Sa frame de départ.
			*/
			int numFrameDepart;

		public:

			//!  Constructeur par défaut. 
			/*!
			  Voici le construteur par défaut.
			*/
			FrequencePotentielle(){}

			//!  Un autre contructeur. 
			/*!
			  Ce constructeur prend des paramètres et permet d'initialiser les fréquences potentielles avec les valeurs voulues.
			*/
			FrequencePotentielle(Real _frequence, int _nbOccurences, int _numFrameDepart):
			 frequence(_frequence), nbOccurences(_nbOccurences), numFrameDepart(_numFrameDepart)
			 {}

			void setFrequence(Real newFrequencies)
			{
				this->frequence = newFrequencies;
			}

			void setNbOccurence(int newNbOccurences)
			{
				this->nbOccurences = newNbOccurences;
			}

			void setNumFrameDepart(int newNumFrameDepart)
			{
				this->numFrameDepart = newNumFrameDepart;
			}

			Real getFrequence()
			{
				return this->frequence;
			}

			int getNbOccurence()
			{
				return this->nbOccurences;
			}

			int getNumFrameDepart()
			{
				return this->numFrameDepart;
			}
		};

		typedef vector<FrequencePotentielle> pitchVector;

		//!  Fonction DirectoryExists. 
		/*!
		  Renvoie true si le path donnée en paramètre existe et false sinon.
		  \param pzPath le chemin à tester.
		  \return Renvoi un booléen
		*/
		bool DirectoryExists( const char* pzPath )
		{
	    	if ( pzPath == NULL) return false;

	    	DIR *pDir;
		    bool bExists = false;

		    pDir = opendir (pzPath);

		    if (pDir != NULL)
		    {
		        bExists = true;
		        (void) closedir (pDir);
		    }

	    	return bExists;
		}

		/////////// DECLARATION DES FONCTIONS AUXILIAIRES ///////////

		//!  Fonction mel700. 
		/*!
		  Convertie les fréquences en mel700.
		  \param frequenceHZ la fréquence en hertz à convertir.
		  \return la fréquence en mel700
		*/
		Real mel700(Real frequenceHZ)
		{
			return (2595 * log10(1 + (frequenceHZ / 700)));
		}


		//!  Fonction estDans. 
		/*!
		  Renvoie l'indice d'une fréquence potentielle si elle est dedans et -1 sinon.
		  \param frequence la fréquence à tester.
		  \param tabFrequ liste dans laquelle il faut vérifier.
		  \param tolerance nombre de frame sautée tolérer.
		  \return l'indice dans le tableau (-1 si pas trouvé)
		*/
		int estDans(Real frequence, vector<FrequencePotentielle> tabFrequ, Real const tolerance)
		{
			for(int k = 0; k < (int) tabFrequ.size(); k++){
	  			// frequenciesTest[i] estdans frequencesPotentielles
	  			if((mel700(tabFrequ[k].getFrequence()) <= mel700(frequence) + tolerance) && (mel700(tabFrequ[k].getFrequence()) >= mel700(frequence) - tolerance)){
	  			//if(tabFrequ[k].getFrequence() == frequence){
	  				return k;
	  			}
	  		}
	  	return -1;
		}


		//!  Fonction verifie. 
		/*!
		  Renvoie l'indice d'une fréquence potentielle si elle est correspond à la tolérence et -1 sinon.
		  \param frequence la fréquence potentielle à tester.
		  \param listeResult liste dans laquelle il faut vérifier.
		  \param tolerance nombre de frame sautée tolérer.
		  \return l'indice dans le tableau (-1 si pas trouvé)
		*/
		int verifie(FrequencePotentielle frequence, vector<FrequencePotentielle> listeResult, Real const tolerance)
		{
			for(int k = 0; k < (int) listeResult.size(); k++){
	  			if(listeResult[k].getNumFrameDepart() == frequence.getNumFrameDepart()){
	  				if((mel700(listeResult[k].getFrequence()) <= mel700(frequence.getFrequence()) + tolerance)
	  					&& (mel700(listeResult[k].getFrequence()) >= mel700(frequence.getFrequence()) - tolerance)){
			  			return k;
			  		}
		  		}
	  		}
	  	return -1;
		}

		//!  Fonction swap. 
		/*!
		  Echange les deux fréquences potetielles passées en paramètre.
		  \param r1 fréquence une.
		  \param r2 fréquence deux.
		*/
		void swap(FrequencePotentielle &r1, FrequencePotentielle &r2)
		{
	  		//pn crée une fréquence potentielle temporaire
			FrequencePotentielle temp = FrequencePotentielle(r1.getFrequence(), r1.getNbOccurence(), r1.getNumFrameDepart());

			r1.setFrequence(r2.getFrequence());
			r1.setNbOccurence(r2.getNbOccurence());
			r1.setNumFrameDepart(r2.getNumFrameDepart());

			r2.setFrequence(temp.getFrequence());
			r2.setNbOccurence(temp.getNbOccurence());
			r2.setNumFrameDepart(temp.getNumFrameDepart());
		}


		//!  Fonction frameToSec. 
		/*!
		  Convertie un numéro de frame en temps en seconde.
		  \param numFrame le numéro de la frame.
		  \param hopsize le nombre de sample qui séparent deux frames.
		  \param sampleRate le nombre de sample par seconde.
		  \return renvoie le temps en secondes
		*/
		float frameToSec(int numFrame, int hopsize, Real sampleRate)
		{
				return 1/(sampleRate / hopsize) * numFrame;
		}



		/////////// CORPS DE L'API ///////////

	//!  Fonction monoLoader. 
		/*!
		  Utilise l'algorithme monoLoader et met le tableau de valeur dans mono.
		  \param audioFilename le path vers le fichier audio que l'on doit convertir en tableau de données.
		*/
	void monoLoader(string audioFilename)
	{

		/////// PARAMS //////////////
		int framesize = 2048;
		int hopsize = 512;
		Real sr = 44100;
  	//Real minSineDur = 0.02;

		//int maxPeaks = atoi(argv[3]);

		//! Factory.
	    /*!
	      On instancie une AlgorithmFactory afin de pouvoir utiliser les algorithmes d'essentia.
	    */
		AlgorithmFactory& factory = AlgorithmFactory::instance();

		//! audioLoader.
	    /*!
	      Instancie l'agorithme MonoLoader (voir doc).
	    */
		Algorithm* audioLoader    = factory.create("MonoLoader",
			"filename", audioFilename,
			"sampleRate", sr,
			"downmix", "mix");

    // putting the analysis results in the analysor attribute mono
		audioLoader->output("audio").set(mono);

		audioLoader->compute();
	}

	//! Fonction detectionFreuenceLongue.
    /*!
      Cette fonction détecte les fréquences qui sont considérées comme longues.
      \param outputFilename le nom du fichier de sortie.
      \param tolerance le nombre de frame que peux sauter une fréquence.
      \param seuilNbOccurence le nombre d'occurence où doit apparaître une fréquence pour être jugées marquante.
      \param seuilFrequency la valeur minimal pour qu'une fréquence soit détcté, si la valeur est trop faible on ne l'entend pas et donc ce n'est pas utile pour le jeu.
      \param maxPeaks le nombre de fréquence utilisées par frame.
    */
	void detectionFrequenceLongue(string outputFilename, int tolerance = 5, int seuilNbOccurence = 20, int seuilFrequency = 200, int maxPeaks = 10 /*int argc, char* argv[]*/)
	{

		if(!DirectoryExists("./out"))
		{
			const int dir_err = mkdir("out", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
			if (dir_err == -1)
			{
    			printf("Error creating directory!n");
   				exit(1);
			}
		}

		//string audioFilename = argv[1];

		Pool pool;

    /////// PARAMS //////////////
		int framesize = 2048;
		int hopsize = 512;
		Real sr = 44100;
  	//Real minSineDur = 0.02;

		//int maxPeaks = atoi(argv[3]);

		//! Factory.
	    /*!
	      On instancie une AlgorithmFactory afin de pouvoir utiliser les algorithmes d'essentia.
	    */
		AlgorithmFactory& factory = AlgorithmFactory::instance();

		//! frameCutter.
	    /*!
	      Instancie l'agorithme FrameCutter (voir doc).
	    */
		Algorithm* frameCutter  = factory.create("FrameCutter",
			"frameSize", framesize,
			"hopSize", hopsize,
        //  "silentFrames", "noise",
			"startFromZero", false );

    // parameters used in the SMS Python implementation

		//! window.
	    /*!
	      Instancie l'agorithme Windowing (voir doc).
	    */
		Algorithm* window       = factory.create("Windowing", "type", "hamming");

		//! fft.
	    /*!
	      Instancie l'agorithme FFT (voir doc).
	    */
		Algorithm* fft     = factory.create("FFT",
			"size", framesize);

    // parameters used in the SMS Python implementation

		//! sinemodelanal.
	    /*!
	      Instancie l'agorithme SineModelAnal (voir doc).
	    */
		Algorithm* sinemodelanal     = factory.create("SineModelAnal",
			"sampleRate", sr,
			"maxnSines", 100,
			"freqDevOffset", 10,
			"freqDevSlope", 0.001,
			"maxPeaks", maxPeaks
			);


		vector<Real> frame;
		vector<Real> wframe;
		vector<complex<Real> > fftframe;

		vector<Real> magnitudes;
		vector<Real> frequencies;
		vector<Real> phases;

		vector< vector<Real> > frequenciesAllFrames;
		vector< vector<Real> > magnitudesAllFrames;
		vector< vector<Real> > phasesAllFrames;

    // analysis

		frameCutter->input("signal").set(mono);
		frameCutter->output("frame").set(frame);

		window->input("frame").set(frame);
		window->output("frame").set(wframe);

		fft->input("frame").set(wframe);
		fft->output("fft").set(fftframe);

    // Sine model analysis
		sinemodelanal->input("fft").set(fftframe);
		sinemodelanal->output("magnitudes").set(magnitudes);
		sinemodelanal->output("frequencies").set(frequencies);
		sinemodelanal->output("phases").set(phases);
	  //-----------------------------------------------
	  // analysis loop
		cout << "-------- analyzing to sine model parameters" " ---------" << endl;
		int counter = 0;
		vector<Real> tampon;
		vector<vector<Real> > frequenciesTest;

		while (true) {
			tampon.clear();

      // compute a frame
			frameCutter->compute();
      // if it was the last one (ie: it was empty), then we're done.
			if (!frame.size()) {
				break;
			}

			window->compute();
			fft->compute();

      // Sine model analysis
			sinemodelanal->compute();


			      // append frequencies of the curent frame for later cleaningTracks
			for(unsigned int i = 0; i < frequencies.size(); i++)
			{
				//if(frequencies[i] != 0){
					tampon.push_back(frequencies[i]);
				//}
			}

			frequenciesAllFrames.push_back(tampon);

			counter++;
		}
	  //-----------------------------------------------

		//cout << frequenciesAllFrames << endl;
	  cout << "-------- Identification fréquences longues ---------" << endl;

	  //RECUP FRAMES POTENTIELLEMENT LONGUES

	  pitchVector tabFrequencesPotentielles; //tableaux de potentielles fréquence longue
	  //Real const tolerance = atoi(argv[4]); //permet de savoir si deux frequences correspondrait a la meme note
	  pitchVector listeResult; //liste des notes qui durent suffisement de frames pour etre remarquable
	  //int const seuil = atoi(argv[5]); //nombre de frame necessaire pour qu'une note soit "remarquable"


	  //tab de booléen pour faire les translations des fréquences
	  //vector<bool> tabBoolFrequenceCourante;

		//boucle qui récupère les fréquences de la premiere frame de la musique, qu'on stocke dans le tableau de fréquence potentielle
		for(int i = 0; i < (int) frequenciesAllFrames[0].size(); i++){
			tabFrequencesPotentielles.push_back(FrequencePotentielle(frequenciesAllFrames[0][i], 1, 0));
		}

		//Pour chaque frame après la première
		for(int i = 1; i < (int) frequenciesAllFrames.size();i++){

			//on crée un tableau temporaire
			vector<FrequencePotentielle> tabTemp = tabFrequencesPotentielles;

			//pour chauqe fréquence potentielle
			for(int j = 0; j <(int) tabFrequencesPotentielles.size(); j++){

				Real frequenceCourante = frequenciesAllFrames[i][j];

				//on vérifie si la fréquence de la musique est déjà présence dans notre table de fréquence temporaire
				int position = estDans(frequenceCourante, tabTemp, tolerance);
				//Si elle est présente
				if(position != -1){
					//on incrémente son occurence de 1
					tabFrequencesPotentielles[j] = FrequencePotentielle(frequenceCourante, tabTemp[position].getNbOccurence() + 1, tabTemp[position].getNumFrameDepart());
				//sinon on la rajoute au tableau de frequance potentielle
				} else{
						tabFrequencesPotentielles[j] = FrequencePotentielle(frequenceCourante, 1, i);

				}

					//si une fréquence est présente un nombre suffisant de fois et qu'elle est suppérieur à une valeur seuil en Hertz
					if(tabFrequencesPotentielles[j].getNbOccurence() >= seuilNbOccurence && tabFrequencesPotentielles[j].getFrequence() >= seuilFrequency){
							//si elle déjà presente dans la liste des résultat, on incrément son nombre d'occurence
					 		int positionResult = verifie(tabFrequencesPotentielles[j], listeResult, tolerance);
							if(positionResult != -1){
								listeResult[positionResult].setNbOccurence(listeResult[positionResult].getNbOccurence() + 1);
							//sinon on la rajoute
							}else{
								listeResult.push_back(tabFrequencesPotentielles[j]);
					}
			 	}
			}
		}

	  delete frameCutter;
	  delete fft;
	  delete sinemodelanal;

		cout << "-------- writing results to file " << outputFilename << " ---------" << endl;
		//const char* outputchar = "./out/"+outputFilename.c_str();
		const char* outputchar = outputFilename.c_str();
		ofstream cout(outputchar);
		for(unsigned i=0;i<listeResult.size();i++){
			cout << frameToSec(listeResult[i].getNumFrameDepart(), hopsize, sr) << " " << frameToSec(listeResult[i].getNbOccurence(), hopsize, sr) << " " << listeResult[i].getFrequence() << endl;
		}
	}

	//! Fonction detectionAmplitudeMarquante.
    /*!
      Cette fonction détecte les sonies qui sont considérées comme marquante.
      \param target savoir si le on utilise le canal mono (tout), right ou left.
      \param outputFilename le nom du fichier de sorti.
      \param frameFraction le nombre de frames que l'on analyse dans un battement.
      \param tauxPrecis le nombre de frames auquel on compare la frame actuelle.
      \param SeuilMarquant le seuil a depasser pour déclarer une fréquence marquante.
      \param sampleRate nombre de sample par seconde.
      \return 0 si tout se passe bien et 1 sinon.
    */
	int detectionAmplitudeMarquante(string target, string outputFilename, /*int frameFraction = 12*/ int frameFraction = 2, int tauxPrecis = 1, float SeuilMarquant = 2, int sampleRate = 44100/* int argc, char* argv[]*/)
	{

	        /*if (argc != 6) {
	                cout << "Error: incorrect number of arguments." << endl;
	                cout << "Usage: " << argv[0] << " audio_input txt_output int_frameFraction int_TauxPrecis float_SeuilMarquant" << endl;
	                exit(1);
	        }*/

	    /////// PARAMS //////////////
      //int sampleRate = 44100;

			/*
			if(!DirectoryExists("./out"))
			{
				const int dir_err = mkdir("out", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
				if (dir_err == -1)
				{
						printf("Error creating directory!n");
						exit(1);
				}
			}
			*/

		//! Factory.
	    /*!
	      On instancie une AlgorithmFactory afin de pouvoir utiliser les algorithmes d'essentia.
	    */
		AlgorithmFactory& factory = standard::AlgorithmFactory::instance();

		//! rytex.
		/*!
		  Instancie l'agorithme RhythmExtractor2013 (voir doc).
		*/
		Algorithm* rytex = factory.create("RhythmExtractor2013");

		/////////// CONNECTING THE ALGORITHMS ////////////////
		cout << "-------- connecting algos ---------" << endl;

			if(target == "left"){
					rytex->input("signal").set(left);
			} else if (target == "right"){
					rytex->input("signal").set(right);
			} else if (target == "mono"){
					rytex->input("signal").set(mono);
			} else {
				cout << "Usage : target ? \"mono\", \"left\" or \"right\"" << endl;
				return 1;
			}

			Real bpm, confidence;
		vector<Real> ticks, estimates, bpmIntervals;

		rytex->output("confidence").set(confidence);
		rytex->output("bpm").set(bpm);
		rytex->output("ticks").set(ticks);
		rytex->output("estimates").set(estimates);
		rytex->output("bpmIntervals").set(bpmIntervals);

		/////////// STARTING THE ALGORITHMS //////////////////

		rytex->compute();

		//int tauxPrecis = atoi(argv[4]);
		//float SeuilMarquant = atof(argv[5]);


		int frameSize = (sampleRate*60/bpm)/frameFraction;
		int hopSize = frameSize;


		cout << "bpm : " << bpm << " _ frameSize : " << frameSize << endl;

		//! levex.
		/*!
		  Instancie l'agorithme LevelExtractor (voir doc).
		*/
		Algorithm* levex    = factory.create("LevelExtractor",
		      "frameSize", frameSize,
		      "hopSize", hopSize);

		vector<Real> loudnesss;

			if(target == "left"){
					levex->input("signal").set(left);
			} else if (target == "right"){
					levex->input("signal").set(right);
			} else {
					levex->input("signal").set(mono);
			}

		levex->output("loudness").set(loudnesss);

		levex->compute();

		vector<Real>  moyLoudness;
		//Début de la musique
		Real moyDeb = 0;
		for(int j=0; j < 2*tauxPrecis*frameFraction+1; j++ ){
		      moyDeb += loudnesss[j];
		}
		moyDeb /= 1 + 2*tauxPrecis*frameFraction;
		for(int i=0; i< tauxPrecis*frameFraction; i++){

		      moyLoudness.push_back(moyDeb);
		}

		//Corps de la musique
		for(unsigned i=tauxPrecis*frameFraction; i< loudnesss.size()-tauxPrecis*frameFraction; i++){
		      Real moyenne = 0;
		      for(unsigned j=i-tauxPrecis*frameFraction; j < i+tauxPrecis*frameFraction; j++ ){
		              moyenne += loudnesss[j];
		      }
		      moyenne -= loudnesss[i];
		      moyenne /= 2*tauxPrecis*frameFraction;
		      moyLoudness.push_back(moyenne);
		}

		//Fin de la musique
		Real moyFin = 0;
		for(unsigned j=loudnesss.size()-(2*tauxPrecis*frameFraction+1); j < loudnesss.size(); j++ ){
		      moyFin += loudnesss[j];
		}
		moyFin /= 1 + 2*tauxPrecis*frameFraction;
		for(int i=0; i< tauxPrecis*frameFraction; i++){

		      moyLoudness.push_back(moyFin);
		}

		  /*
		  string test = "vlc " + audioFilename + "&";
		  const char* audioplay = test.c_str();
		  system(audioplay);

		  cout << loudnesss.size() << endl;
		  for(unsigned i=0; i< loudnesss.size(); i++){
		    usleep((60/bpm/frameFraction)*1000000);
		    if(loudnesss[i] > 1){
		      if(loudnesss[i] > 1.85*moyLoudness[i]){
		        cout << "===> LOUDNESS MARQUANTE <===  : " << loudnesss[i] << " ( moyLoudness = " << moyLoudness[i] << " )" << endl;
		      }else{
		        //cout << "loudness : " << loudnesss[i] << " ( moyLoudness = " << moyLoudness[i] << " )" << endl;
		      }
		    }else{
		      cout << "-------------------- Frame Silencieuse --------------------" << endl;

		    }
		  }*/

		cout << "-------- writing results to file " << outputFilename << " ---------" << endl;
		//const char* outputchar = "./out/"+outputFilename.c_str();
		const char* outputchar = outputFilename.c_str();
		ofstream cout(outputchar);
		for(unsigned i=0;i<loudnesss.size();i++){
		  //on teste si la fréquence est marquante ou non, puis on l'écrit dans le fichier
		  if(loudnesss[i] > SeuilMarquant*moyLoudness[i]){
		      cout << i*(60/bpm/frameFraction) << " " << loudnesss[i] << " 1" << endl; // tmp (sec) : loudness(value) : boolean (marquant?)
		    }else{
		      cout << i*(60/bpm/frameFraction) << " " << loudnesss[i] << " 0" << endl; // tmp (sec) : loudness(value) : boolean (marquant?)
		    }
		}

		delete rytex;
		delete levex;	

		return 0;
	}

	//! Fonction metaDataReader.
    /*!
      Cette fonction extrait les méta données d'une musique.
      \param filename nom du fichier à traiter.
    */
	void metaDataReader(string filename)
	{

		/////// PARAMS //////////////

		//! Factory.
	    /*!
	      On instancie une AlgorithmFactory afin de pouvoir utiliser les algorithmes d'essentia.
	    */
		AlgorithmFactory& factory = standard::AlgorithmFactory::instance();

		//! metred.
	    /*!
	      Instancie l'agorithme MetadataReader (voir doc).
	    */
		Algorithm* metred  = factory.create("MetadataReader",
		                                  "filename", filename);


		/////////// CONNECTING THE ALGORITHMS ////////////////

		string title, artist, album, comment, genre, tracknumber, date;
		int duration, bitrate, sampleRate, channels;
		Pool tagPool;

		metred->output("title").set(title);
		metred->output("artist").set(artist);
		metred->output("album").set(album);
		metred->output("comment").set(comment);
		metred->output("genre").set(genre);
		metred->output("tracknumber").set(tracknumber);
		metred->output("date").set(date);
		metred->output("tagPool").set(tagPool);
		metred->output("duration").set(duration);
		metred->output("bitrate").set(bitrate);
		metred->output("sampleRate").set(sampleRate);
		metred->output("channels").set(channels);


		/////////// STARTING THE ALGORITHMS //////////////////

		metred->compute();

		delete metred;

		datas = metaData(title, artist, album, comment, genre, tracknumber, date, duration, bitrate, sampleRate, channels);
	}

	//! Fonction stereoLoader.
    /*!
      Cette foncion sépare la musique en tableau gauche et droite.
      \param filename nom du fichier à traiter.
    */
	void stereoLoader(string filename)
	{

	    /////// PARAMS //////////////

		//! Factory.
	    /*!
	      On instancie une AlgorithmFactory afin de pouvoir utiliser les algorithmes d'essentia.
	    */
		AlgorithmFactory& factory = standard::AlgorithmFactory::instance();

		//! audio.
		/*!
		  Instancie l'agorithme AudioLoader (voir doc).
		*/
		Algorithm* audio = factory.create("AudioLoader",
		                                "filename", filename);

		/////////// CONNECTING THE ALGORITHMS ////////////////
		cout << "-------- connecting AudioLoader ---------" << endl;


		vector<StereoSample> audioBuffer;
		Real sampleRate;
		int numberChannels, bit_rate ;
		string md5, codec;


		audio->output("audio").set(audioBuffer);
		audio->output("sampleRate").set(sampleRate);
		audio->output("numberChannels").set(numberChannels);
		audio->output("md5").set(md5);
		audio->output("bit_rate").set(bit_rate);
		audio->output("codec").set(codec);


		/////////// STARTING THE ALGORITHMS //////////////////
		cout << "-------- start processing StereoDemuxer" << filename << " --------" << endl;

		audio->compute();


		cout << "Nombre de canaux : " << numberChannels << endl;


		if(numberChannels == 2){


		  Algorithm* stedem = factory.create("StereoDemuxer");


		  /////////// CONNECTING THE ALGORITHMS ////////////////
		  cout << "-------- connecting StereoDemuxer ---------" << endl;


		  stedem->input("audio").set(audioBuffer);
		  stedem->output("left").set(left);
		  stedem->output("right").set(right);


		  /////////// STARTING THE ALGORITHMS //////////////////
		  cout << "-------- start processing StereoDemuxer " << filename << " --------" << endl;

		  stedem->compute();

		  cout << "Fichier audio séparé !" << endl;

		  delete stedem;

		} else {
			cout << "File contains incorrect number of channels" << endl;
		}


		delete audio;
	}

	//! Fonction stereoFileMerger.
    /*!
      Cette foncion fusionne les données traitées du coté gauche et droit.
      \param rightFilename nom du fichier de droite.
      \param leftFilename nom du fichier de gauche.
      \param outputFilename nom du fichier de sortie.
    */
	void stereoFileMerger(string rightFilename, string leftFilename, string outputFilename){

			ifstream inputLeft(leftFilename.c_str());

			vector<float> timesLeft, timesRight;
			vector<float> loudnessesLeft, loudnessesRight;
			vector<int> marquantesLeft, marquantesRight;

			string strLeft, strRight;

			float timeLeft, timeRight;
		  float loudnessLeft, loudnessRight;
		  char miss;
		  int marquantLeft, marquantRight;

		  while(getline(inputLeft, strLeft)){

		  	istringstream istrLeft(strLeft);
		    istrLeft >> timeLeft >> miss >> loudnessLeft >> miss >> marquantLeft;

				timesLeft.push_back(timeLeft);
		  	loudnessesLeft.push_back(loudnessLeft);
		    marquantesLeft.push_back(marquantLeft);

		  }
			inputLeft.close();

			ifstream inputRight(rightFilename.c_str());

			while(getline(inputRight, strRight)){

				istringstream istrRight(strRight);
				istrRight >> timeRight >> miss >> loudnessRight >> miss >> marquantRight;

				timesRight.push_back(timeRight);
				loudnessesRight.push_back(loudnessRight);
				marquantesRight.push_back(marquantRight);

		  }
			inputRight.close();


			cout << "-------- merging files to " << outputFilename << " ---------" << endl;

			const char* outputchar = outputFilename.c_str();
			ofstream cout(outputchar);

			unsigned int index = 0;

			while(index < min(loudnessesLeft.size(),loudnessesRight.size())){
						cout << (timesLeft[index] + timesRight[index])/2 << " "
						<< max(loudnessesLeft[index], loudnessesRight[index]) << " "
						<< max(marquantesLeft[index], marquantesRight[index]) << " "
						<< fabs(loudnessesLeft[index] - loudnessesRight[index]) << endl;
						// tmp (sec) loudness(value) boolean (marquant?) difference
						index++;
			}
			if(index == loudnessesLeft.size()){
				for(unsigned int i = index; i < loudnessesRight.size(); i++){
					cout << timesRight[i] << " "
					<< loudnessesRight[i] << " "
					<< marquantesRight[i] << " "
					<< loudnessesRight[i] << endl;
				}
			} else {
				for(unsigned int i = index; i < loudnessesLeft.size(); i++){
					cout << timesLeft[i] << " "
					<< loudnessesLeft[i] << " "
					<< marquantesLeft[i] << " "
					<< loudnessesLeft[i] << endl;
				}
			}
		}


	};

	#endif
