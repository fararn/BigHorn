#include <iostream>
#include <fstream>
#include <sstream>
#include <fstream>
#include <algorithm>

#include "Image2D.hpp"
#include "Image2DWriter.hpp"

#define MAXLOUDNESS 200

using namespace std;

int myScaleLoudness(float ratio, float loudness){
  return (int)(ratio*loudness);
}

int myScaleTime(float time){
  return (int)(time*10);
}

int main(int argc, char* argv[])
{
	typedef Image2D<unsigned char> GrayLevelImage2D;
  typedef Image2DWriter<unsigned char> GrayLevelWriter;
	string str;
	vector<float> times;
	vector<float> loudnesses;
  vector<int> marquantes;  
  if ( argc < 3 ) 
  { 
    cerr << "Usage: Visualisation <input file ( sec : loudness )> <output.ppm>" << endl;
    return 0;
  }

  ifstream input( argv[1] ); // récupère le 1er argument.
  int nbValeurs = 0;
  float time;
  float loudness;
  char miss;
  int marquant;

  while(getline(input, str)){

  	istringstream istr(str);
    istr >> time >> miss >> loudness >> miss >> marquant;

  	times.push_back(time);
  	loudnesses.push_back(loudness);
    marquantes.push_back(marquant);
    cout << marquant << endl;
  	nbValeurs++;
  }
  input.close();
  
  float maxLoudness = *max_element(loudnesses.begin(), loudnesses.end());
  float ratio = MAXLOUDNESS/maxLoudness;
  float lastTime = times[nbValeurs-1];

  //auto myScaleLoudness = [ratio](float l){return (int)ratio*l; };
  //auto myScaleTime = [](float t){return (int)t*10; };

  GrayLevelImage2D img = GrayLevelImage2D(myScaleTime(lastTime), MAXLOUDNESS, 255);

  for(int indice = 0; indice < nbValeurs; indice++){
    if(marquantes[indice] == 0){
      for(int indice_loudness = MAXLOUDNESS - myScaleLoudness(ratio, loudnesses[indice]); indice_loudness < MAXLOUDNESS; indice_loudness++){
        if(img.at(myScaleTime(times[indice]), indice_loudness) != 0)
          img.at(myScaleTime(times[indice]), indice_loudness) = (unsigned char) 125;
      }
    } else {
      for(int indice_loudness = MAXLOUDNESS - myScaleLoudness(ratio, loudnesses[indice]); indice_loudness < MAXLOUDNESS; indice_loudness++){
        img.at(myScaleTime(times[indice]), indice_loudness) = (unsigned char) 0;
      }
    }
  }

  ofstream output( argv[2] );
  if(!GrayLevelWriter::write( img, output, false )){
      cerr << "Error writing output file." << std::endl;
      return 1;
  }

  output.close();

  return 0;
}