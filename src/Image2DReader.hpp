#ifndef _IMAGE2DREADER_HPP_
#define _IMAGE2DREADER_HPP_

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include "Color.hpp"
#include "Image2D.hpp"

template <typename TValue>
class Image2DReader {
public:
  typedef TValue Value;
  typedef Image2D<Value> Image;
  static bool read( Image & img, std::ostream & input )
  {
    std::cerr << "[Image2DReader<TValue>::read] NOT IMPLEMENTED." << std::endl;
    return false;
  }
};
/// Specialization for gray-level images.
template <>
class Image2DReader<unsigned char> {
public:
  typedef unsigned char Value;
  typedef Image2D<Value> Image;
  typedef Image2D<Value>::Iterator Iterator;
  static bool read( Image & img, std::istream & input )
  {
    std::string str;
    unsigned char tmp;
    int tmpW;
    int tmpH;
    if ( ! input.good() ){
      std::cerr << "Probleme !"<<std::endl;
      return 0;
    }
    else {
      std::getline( input, str );
      if(str == "P2"){
        tmp = (int) tmp;
        std::getline( input, str );
      }
      std::getline( input, str );
      std::getline( input, str );
      input >> tmpW;
      input >> tmpH;
      img = Image(tmpW, tmpH);
      input >> tmp; //pour sauter le niveau max;
      for ( Iterator it = img.begin(), itE = img.end(); it != itE; ++it )
      {
        input >> tmp;
        *it = tmp;
      }
      return 1;
    }
  }
};
/// Specialization for color images.
template <>
class Image2DReader<Color> {
public:
  typedef Color Value;
  typedef Image2D<Value> Image;
  typedef Image2D<Value>::Iterator Iterator;
  static bool read( Image & img, std::istream & input){
    std::string str;
    int tmpW;
    int tmpH;
    unsigned char tmpR;
    unsigned char tmpG;
    unsigned char tmpB;
    if ( ! input.good() ){
     return 0;
    }
    else {
      std::getline( input, str );
      if(str == "P3"){
        tmpR = (int) tmpR;
        tmpG = (int) tmpG;
        tmpB = (int) tmpB;
        std::getline( input, str );
      }

      std::getline( input, str );
      //std::getline( input, str );

      std::istringstream istr(str);
      istr >> tmpW >> tmpH;

      img = Image(tmpW, tmpH, Color(0,0,0));
      std::getline( input, str ); //pour sauter le niveau max;

      input >> std::noskipws;
      for ( Iterator it = img.begin(), itE = img.end(); it != itE; ++it )
      {
        input >> tmpR >> tmpG >> tmpB;
        *it = Color( tmpR, tmpG, tmpB );
      }
    }
    return 1;
  }
};
#endif // _IMAGE2DREADER_HPP_
