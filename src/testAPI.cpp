#include <iostream>
#include "Analyseur.hpp"

using namespace std;


int main(int argc, char* argv[]){


  if (argc <= 2) {
  	cout << "Standard_SineModel Error: incorrect number of arguments." << endl;
  	cout << "Usage (at least): " << argv[0] << " audio_input output_file precision tauxPrecision seuilMarquant" << endl;
  	exit(1);
  }
   
	Analyseur analysor;
  string filename = argv[1];

	cout << "-------- Detection des frequences longues --------" << endl;

	Analyseur::pitchVector frequencesLongues = analysor.detectionFrequenceLongue(filename);

  //Affichage des fréquences marquantes
    for(unsigned int i = 0; i < frequencesLongues.size(); i++)
    {
      cout << "frequencesLongues[" << i << "] : Frequence = " 
      << frequencesLongues[i].getFrequence()
      << " - Frame depart = " << frequencesLongues[i].getNumFrameDepart()
      << " - nb occurence = "<< frequencesLongues[i].getNbOccurence()
      << endl;
    }


	cout << "-------- Detection des amplitudes marquantes --------" << endl;


	analysor.detectionAmplitudeMarquante(filename, argv[2] /*, atoi(argv[3]), atoi(argv[4]), atof(argv[5])*/);

  cout << "-------- Lecture des meta donnees --------" << endl;

  analysor.metaDataReader(filename).affichage();

  /*
  vector<Real> audioL = analysor.stereo(argv[1]).left;
  vector<Real> audioR = analysor.stereo(argv[1]).right;
  for(unsigned int i = 0; i < audioL.size(); i++){
    cout << audioL[i] << "      " << audioR[i] << endl;  
  }
  */

  analysor.~Analyseur();
	
  return 0;
}